;;; paraulogic --- Troba paraules del paraulogic

;;; Commentary:
;;; Necessita un bon diccionari

;;; Code:

;;(defvar *dict* (list "orgull" "rotllo" "furor" "desgavell"))
(defvar *dict*
  (with-current-buffer (find-file-noselect "./catala.dic")
    (split-string
     (buffer-substring-no-properties (point-min) (point-max))
     "\n"))
  "Dictionary as a list of strings.")

;; The 'paraulogic' required characters
(defvar *good* "gdaevl"
  "Set of limited letters to use, but the required one, as a string.
Case insensitive.")

(defvar *required-char* "s"
  "The required char in every word, case insensitive.")

;; The result goes to *Messages* buffer
(let ((char-required-found nil)
      (count-words 0))
  (dolist (word *dict*)
    (catch 'next-word
      (when (< (length word) 3)
        (throw 'next-word nil))
      (setq char-required-found nil)
      (dolist (c (append word nil))
        (catch 'next-character
          (when (string= (downcase (string c)) (downcase *required-char*))
            (setq char-required-found t)
            (throw 'next-character nil))
          (dolist (g (append *good* nil))
            (when (string= (downcase (string c)) (downcase (string g)))
              (throw 'next-character nil)))
          (throw 'next-word nil)))
      (when char-required-found
        (setq count-words (1+ count-words))
        (message "%d: %s" count-words word)))))

;; This code could be cleaner but slow, because does not shortcircuits on
;; pointless branches.
;; (let ((word-found nil)
;;       (char-good-found nil)
;;       (char-required-found nil)
;;       (count-words 0))
;;   (dolist (word *dict*)
;;     (setq word-found t)
;;     (setq char-required-found nil)
;;     (when (>= (length word) 3)
;;       (dolist (c (append word nil))
;;         (setq char-good-found nil)
;;         (if (string= (downcase (string c)) (downcase *required-char*))
;;             (setq char-required-found t)
;;           (dolist (g (append *good* nil))
;;             (when (string= (downcase (string c)) (downcase (string g)))
;;               (setq char-good-found t)))
;;           (when (not char-good-found)
;;             (setq word-found nil)))))
;;     (when (and word-found char-required-found)
;;       (setq count-words (1+ count-words))
;;       (message "%d: %s" count-words word))))

(provide 'paraulogic)
;;; paraulogic.el ends here
