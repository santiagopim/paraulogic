# Paraulogic

Code used to find words in a dictionary that contain the letters you say.

## Usage

Just define the `*good*` and `*required-char*` variables, compile and
evaluate with `emacs-lisp-byte-compile-and-load` or with `C-M-x` on
every variable definition and on the last form.

## Algorithm

For every word in dictionary list:

1. Check that word has length greater or equal 3, if not jump to the
   next word.
2. For every character of word:
   - Check if the character is the same as the required one, if so
     jump to the next character in word.
   - For every letter in the letters to use:
       + Check if the character in word is the same as the letter, if
         so jump to the next character in word.
   - Finally the character is not the required one nor equals any
     letter, so jump to next word.
3. Finally if the word has the required letter, print it.
   
## License

For open source projects, say how it is licensed.


