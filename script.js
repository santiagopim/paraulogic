/*!
 * Desenvolupat per Pere Orga <pere@orga.cat>, 2021.
 * Originalment basat en https://github.com/lzha97/spelling_bee (Apache License 2.0).
 * Inclou la lletra Open Sans (Apache License 2.0), imatges del projecte Noto Emoji (SIL Open Font License 1.1) i icones dels projectes Font Awesome (Creative Commons Attribution 4.0) i Bootstrap (MIT License).
 * Alguns sons s'han extret de https://freesound.org/people/DrinkingWindGames/sounds/572954/, https://freesound.org/people/soundbytez/sounds/111074/ i https://freesound.org/people/Benboncan/sounds/64544/, tots tres amb llicència Creative Commons Attribution 3.0.
 */
function f(e, t = null, n = !1) {
    t && t.preventDefault(), -1 !== e.indexOf(" o ") && (e = e.substring(e.lastIndexOf(" o ") + 3));
    let o = new XMLHttpRequest();
    o.overrideMimeType("application/json"),
        o.open("GET", "/?diec=" + e, !0),
        o.setRequestHeader("Authorization", "Basic Y29udHJhc2VueWE="),
        (o.onload = function () {
            let e = JSON.parse(o.responseText);
            if ("d" in e) {
                let o = e.d + '<div class="copyright">© Institut d’Estudis Catalans</div>';
                if (n) {
                    document.querySelectorAll(".sol-def").forEach(function (e) {
                        e.style.display = "none";
                    });
                    let e = t.target.parentElement.querySelectorAll(".sol-def")[0];
                    (e.innerHTML = o), (e.style.display = "block");
                    let n = e.parentElement,
                        l = n.offsetTop - n.scrollTop + n.clientTop;
                    (e.style.top = l + 30 + "px"), e.getBoundingClientRect().bottom > (window.innerHeight || document.documentElement.clientHeight) && (e.style.top = l - 300 + "px");
                } else document.getElementById("definition").innerHTML = o;
            }
        }),
        o.send(null);
}
function doTooltip() {}
function hideTip() {}
function getFullAccepcio() {}
!(function () {
    let e = 0,
        t = 0,
        n = 0,
        o = 0,
        l = 0,
        i = {},
        c = [],
        s = "",
        r = "",
        d = !0;
    const a = ["", "pollet", "colom", "anec", "cigne", "oliba", "aguila"];
    function u(e) {
        if (!e) return 0;
        let t = (e / o) * 100;
        return t < 5 ? 1 : t < 10 ? 2 : t < 20 ? 3 : t < 40 ? 4 : t < 70 ? 5 : 6;
    }
    function m() {
        let e = document.getElementById("hex-grid");
        e.innerHTML = null;
        for (let n = 0; n < s.length; n++) {
            let o = s[n],
                l = document.createElement("P");
            l.innerHTML = o;
            let i = document.createElement("A");
            (i.className = "hex-link"),
                (i.draggable = !1),
                i.appendChild(l),
                i.addEventListener("click", function (e) {
                    document.getElementById("test-word").innerHTML += e.target.innerHTML;
                    let t = document.getElementById("message");
                    (t.style.transitionDuration = "0s"), t.classList.add("hide"), t.classList.remove("show");
                });
            let c = document.createElement("DIV");
            (c.className = "hex-in"), c.appendChild(i);
            let r = document.createElement("LI");
            (r.className = "hex"), r.appendChild(c), n === t && (i.id = "center-letter"), e.appendChild(r);
        }
    }
    function g() {
        h(), (document.getElementById("definition").innerHTML = ""), s.shuffle();
        let e = s.indexOf(r);
        if (s[t] != r) {
            let n = s[t];
            (s[t] = r), (s[e] = n);
        }
        m();
    }
    function p() {
        let e = document.getElementById("test-word"),
            t = e.innerHTML.substring(0, e.innerHTML.length - 1);
        e.innerHTML = t;
    }
    function y(e, t = "") {
        let n = document.getElementById("message");
        (n.innerHTML = e), (n.style.transitionDuration = ""), n.classList.remove("hide-initial"), n.classList.remove("hide"), n.classList.remove("message-ok");
        let o = 1e3;
        t && ((o = 1500), n.classList.add("message-ok"), (n.innerHTML += t), t.length > 4 && (o = 2500)),
            setTimeout(function () {
                n.classList.add("hide"), n.classList.remove("show");
            }, o),
            h();
    }
    function h() {
        document.getElementById("test-word").innerHTML = "";
    }
    function E(t = null, d = !0) {
        let m = null !== t,
            g = m ? t : document.getElementById("test-word").innerHTML;
        if (!g.length) return;
        if ((m || (document.getElementById("definition").innerHTML = ""), g.length < e)) return void (m || y("Massa curt"));
        if (c.includes(g)) return void (m || (y("Ja hi és"), localStorage.getItem("def") && f(i[g])));
        if (!g.includes(r)) return void (m || y("Falta la lletra del mig"));
        if (!Object.prototype.hasOwnProperty.call(i, g)) {
            if (!m) {
                for (let e = 0; e < g.length; e++) if (!s.includes(g[e])) return void y("Lletres incorrectes");
                y("Incorrecte");
            }
            return;
        }
        c.push(g), m || localStorage.setItem("words_" + document.body.getAttribute("data-joc"), JSON.stringify(c));
        let p = L(g, s);
        p && document.body.classList.add("pangrama");
        let h = 0;
        d && (h = u(n));
        let E = I(g, p);
        if (((n += E), !m && localStorage.getItem("def") && f(i[g]), d)) {
            c.length === l && alert("L'enhorabona! Has trobat tots els mots possibles. Gràcies per jugar-hi."),
                c.sort(function (e, t) {
                    return e.localeCompare(t);
                });
            let e = "";
            for (let t = 0; t < c.length; t++) {
                e ? (e += ", ") : (e = ": ");
                let n = "show-def";
                L(c[t], s) && (n += " pangrama"), (e += "<strong class='" + n + "' onclick='f(\"" + i[c[t]] + "\", event);'>" + i[c[t]] + "</strong>");
            }
            (e += "."),
                (document.getElementById("discovered-text").innerHTML = e),
                (document.getElementById("letters-found").innerHTML = c.length),
                (document.getElementById("found-suffix").innerHTML = 1 === c.length ? "paraula" : "paraules");
            let t = u(n),
                r = a[t];
            if (
                (localStorage.getItem("mute")
                    ? (document.getElementById("level").innerHTML = "<img src='img/" + r + ".svg' alt='" + r + "' width='32' height='32'>")
                    : (document.getElementById("level").innerHTML =
                          "<div onclick='this.firstElementChild.play();'><audio id='audio' src='mp3/" + r + ".mp3' preload='auto'></audio><img src='img/" + r + ".svg' alt='" + r + "' width='32' height='32'></div>"),
                (document.getElementById("stars").innerHTML =
                    "<span aria-label='Nivell " +
                    t +
                    "'>" +
                    (function (e) {
                        let t = "";
                        for (let n = 0; n < e; n++) t += "★";
                        if (localStorage.getItem("expert")) for (let n = e; n < 6; n++) t += "✩";
                        return t;
                    })(t) +
                    "</span>"),
                localStorage.getItem("expert") && ((document.getElementById("found-suffix").innerHTML += " de les " + l + " possibles"), (document.getElementById("score").innerHTML = "Puntuació: " + n + "/" + o)),
                !m)
            ) {
                let e = "",
                    o = " +" + E;
                p ? ((e = "Tuti!"), ga("send", "event", "Level", "Tuti", g)) : (e = g.length < 5 ? "Bé!" : g.length < s.length ? "Molt bé!" : "Esplèndid!"),
                    t !== h
                        ? (ga("send", "event", "Level", "Up", r),
                          t > 1 &&
                              ((o += ". Has pujat de nivell!"),
                              localStorage.getItem("mute") ||
                                  setTimeout(function () {
                                      document.getElementById("audio").play();
                                  }, 250)))
                        : t != u(n + 2)
                        ? (o += ". Et falta poc per pujar de nivell!")
                        : t != u(n + 1) && (o += ". Et falta molt poc per pujar de nivell!"),
                    y(e, o);
            }
        }
    }
    function I(e, t) {
        return e.length > 4 ? (t ? e.length + 10 : e.length) : 4 === e.length ? 2 : 1;
    }
    function L(e, t) {
        let n = 0;
        for (let o = 0; o < t.length; o++) e.includes(t[o]) && n++;
        return n === s.length;
    }
    function T() {
        document.querySelectorAll(".modal").forEach(function (e) {
            (e.style.display = "none"), (document.body.style.overflow = "auto");
        }),
            document.activeElement.blur();
    }
    (Array.prototype.shuffle = function () {
        let e = this;
        for (let t = e.length - 1; t >= 0; t--) {
            let n = Math.floor(Math.random() * (t + 1)),
                o = e[n];
            (e[n] = e[t]), (e[t] = o);
        }
        return e;
    }),
        (document.body.onkeydown = function (e) {
            !(function (e) {
                if (13 === e.keyCode) E();
                else if (8 === e.keyCode) e.preventDefault(), p();
                else if (27 === e.keyCode) e.preventDefault(), T(), g();
                else if (191 === e.keyCode || 220 === e.keyCode) {
                    document.getElementById("test-word").innerHTML += "ç";
                    let e = document.getElementById("message");
                    (e.style.transitionDuration = "0s"), e.classList.add("hide"), e.classList.remove("show");
                } else if (e.keyCode >= 65 && e.keyCode <= 90) {
                    document.getElementById("test-word").innerHTML += String.fromCharCode(e.keyCode).toLowerCase();
                    let t = document.getElementById("message");
                    (t.style.transitionDuration = "0s"), t.classList.add("hide"), t.classList.remove("show");
                }
            })(e);
        }),
        (document.getElementById("submit-button").onclick = function () {
            E();
        }),
        (document.getElementById("delete-button").onclick = function () {
            p();
        }),
        (document.getElementById("shuffle-button").onclick = function () {
            g();
        }),
        document.querySelectorAll(".close-button").forEach(function (e) {
            e.addEventListener("click", function (e) {
                T();
            });
        }),
        document.querySelectorAll(".close-icon-link").forEach(function (e) {
            e.addEventListener("click", function (e) {
                e.preventDefault(), T();
            });
        }),
        (document.getElementById("instructions-link").onclick = function (e) {
            e.preventDefault(), (document.getElementById("instruccions").style.display = "block"), (document.getElementById("instruccions").scrollTop = 0), (document.body.style.overflow = "hidden");
        }),
        (document.getElementById("solution-link").onclick = function (e) {
            e.preventDefault();
            let t = document.body.getAttribute("data-joc-ant"),
                n = JSON.parse(localStorage.getItem("words_" + t)),
                o = document.getElementById("solution-list"),
                l = document.getElementById("words-yesterday"),
                i = new XMLHttpRequest();
            i.overrideMimeType("application/json"),
                i.open("GET", "/?solucions=" + t, !0),
                i.setRequestHeader("Authorization", "Basic Y29udHJhc2VueWE="),
                (i.onload = function () {
                    let e = JSON.parse(i.responseText),
                        t = e.lletres,
                        c = e.paraules;
                    (l.innerHTML = "<strong>" + t[t.length - 1] + "</strong> + "),
                        (l.innerHTML += t
                            .slice(0, -1)
                            .sort(function (e, t) {
                                return e.localeCompare(t);
                            })
                            .join("")),
                        (o.innerHTML = null);
                    let s = 0,
                        r = 0;
                    for (let e in c)
                        if (Object.prototype.hasOwnProperty.call(c, e)) {
                            s++;
                            let l = document.createElement("LI");
                            l.onclick = function () {
                                document.querySelectorAll(".sol-def").forEach(function (e) {
                                    e.style.display = "none";
                                });
                            };
                            let i = c[e],
                                d = "";
                            (d = L(e, t) ? "<strong class='show-def pangrama' onclick='f(\"" + i + "\", event, true);'>" + i + "</strong>" : "<span class='show-def' onclick='f(\"" + i + "\", event, true);'>" + i + "</span>"),
                                n && n.includes(e) && (r++, (d = '<img src="img/check.svg" class="ok-icon" title="Trobat" alt="Trobat">' + d)),
                                (l.innerHTML = d += "<span class='sol-def'></span>"),
                                o.appendChild(l);
                        }
                    (document.getElementById("found-yesterday").innerHTML = r
                        ? 1 === r
                            ? "Ahir vas trobar 1 paraula de les " + s + " possibles."
                            : "Ahir vas trobar " + r + " paraules de les " + s + " possibles."
                        : "Ahir es podien trobar " + s + " paraules."),
                        (document.getElementById("solucio").style.display = "block"),
                        (document.getElementById("solucio").scrollTop = 0),
                        (document.body.style.overflow = "hidden");
                }),
                i.send(null);
        }),
        (function () {
            let n = new XMLHttpRequest();
            n.overrideMimeType("application/json"),
                n.open("GET", "/?solucions=" + document.body.getAttribute("data-joc"), !0),
                n.setRequestHeader("Authorization", "Basic Y29udHJhc2VueWE="),
                (n.onload = function () {
                    let c = JSON.parse(n.responseText);
                    (s = c.lletres), (i = c.paraules), (e = c.min), (t = Math.floor(s.length / 2)), (r = s[s.length - 1]), (s[s.length - 1] = s[t]), (s[t] = r);
                    for (let e in i) Object.prototype.hasOwnProperty.call(i, e) && (l++, (o += I(e, L(e, s))));
                    m(),
                        (function () {
                            let e = JSON.parse(localStorage.getItem("words_" + document.body.getAttribute("data-joc")));
                            if (e) for (let t = 0; t < e.length; t++) E(e[t], t === e.length - 1);
                        })();
                }),
                n.send(null);
        })(),
        setInterval(function () {
            d ? ((document.getElementById("cursor").style.opacity = 0), (d = !1)) : ((document.getElementById("cursor").style.opacity = 1), (d = !0));
        }, 600);
    let v = document.getElementById("expert");
    (v.onclick = function () {
        localStorage.setItem("expert", this.checked ? "1" : ""),
            setTimeout(function () {
                location.reload();
            }, 250);
    }),
        (v.checked = !!localStorage.getItem("expert"));
    let B = document.getElementById("def");
    B &&
        ((B.onclick = function () {
            localStorage.setItem("def", this.checked ? "1" : ""),
                setTimeout(function () {
                    location.reload();
                }, 250);
        }),
        (B.checked = !!localStorage.getItem("def")));
    let b = document.getElementById("mute");
    (b.onclick = function () {
        localStorage.setItem("mute", this.checked ? "1" : ""),
            setTimeout(function () {
                location.reload();
            }, 250);
    }),
        (b.checked = !!localStorage.getItem("mute"));
})();
